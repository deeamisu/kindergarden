<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200924002518 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE sub_menu_item (id INT AUTO_INCREMENT NOT NULL, menu_item_id INT NOT NULL, sub_menu_item_route_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_C55993EE9AB44FE0 (menu_item_id), INDEX IDX_C55993EEBEBF3B8E (sub_menu_item_route_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sub_menu_item ADD CONSTRAINT FK_C55993EE9AB44FE0 FOREIGN KEY (menu_item_id) REFERENCES menu_item (id)');
        $this->addSql('ALTER TABLE sub_menu_item ADD CONSTRAINT FK_C55993EEBEBF3B8E FOREIGN KEY (sub_menu_item_route_id) REFERENCES menu_item_route (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE sub_menu_item');
    }
}
