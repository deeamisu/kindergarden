<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200927114611 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE menu_item DROP FOREIGN KEY FK_D754D550FE2229BB');
        $this->addSql('ALTER TABLE sub_menu_item DROP FOREIGN KEY FK_C55993EEBEBF3B8E');
        $this->addSql('DROP TABLE menu_item_route');
        $this->addSql('DROP TABLE sub_menu_item');
        $this->addSql('DROP INDEX IDX_D754D550FE2229BB ON menu_item');
        $this->addSql('ALTER TABLE menu_item DROP menu_item_route_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE menu_item_route (id INT AUTO_INCREMENT NOT NULL, route VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE sub_menu_item (id INT AUTO_INCREMENT NOT NULL, menu_item_id INT NOT NULL, sub_menu_item_route_id INT DEFAULT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, INDEX IDX_C55993EE9AB44FE0 (menu_item_id), INDEX IDX_C55993EEBEBF3B8E (sub_menu_item_route_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE sub_menu_item ADD CONSTRAINT FK_C55993EEBEBF3B8E FOREIGN KEY (sub_menu_item_route_id) REFERENCES menu_item_route (id)');
        $this->addSql('ALTER TABLE sub_menu_item ADD CONSTRAINT FK_C55993EE9AB44FE0 FOREIGN KEY (menu_item_id) REFERENCES menu_item (id)');
        $this->addSql('ALTER TABLE menu_item ADD menu_item_route_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE menu_item ADD CONSTRAINT FK_D754D550FE2229BB FOREIGN KEY (menu_item_route_id) REFERENCES menu_item_route (id)');
        $this->addSql('CREATE INDEX IDX_D754D550FE2229BB ON menu_item (menu_item_route_id)');
    }
}
