<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200927212809 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE gallery_item_gallery');
        $this->addSql('ALTER TABLE gallery_item DROP image_slide');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE gallery_item_gallery (gallery_item_id INT NOT NULL, gallery_id INT NOT NULL, INDEX IDX_D096548E2A151376 (gallery_item_id), INDEX IDX_D096548E4E7AF8F (gallery_id), PRIMARY KEY(gallery_item_id, gallery_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE gallery_item_gallery ADD CONSTRAINT FK_D096548E4E7AF8F FOREIGN KEY (gallery_id) REFERENCES gallery (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE gallery_item_gallery ADD CONSTRAINT FK_D096548E2A151376 FOREIGN KEY (gallery_item_id) REFERENCES gallery_item (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE gallery_item ADD image_slide VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
