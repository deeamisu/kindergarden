<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200927221238 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE slot (id INT AUTO_INCREMENT NOT NULL, slot_badge_id INT DEFAULT NULL, INDEX IDX_AC0E206726F7A36F (slot_badge_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE slot_background_color (id INT AUTO_INCREMENT NOT NULL, slot_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, color_id VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_5369E12059E5119C (slot_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE slot_badge (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, badge_id VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE slot_item (id INT AUTO_INCREMENT NOT NULL, slot_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, image VARCHAR(255) DEFAULT NULL, link VARCHAR(255) NOT NULL, text VARCHAR(255) NOT NULL, INDEX IDX_C1328F8959E5119C (slot_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE slot_item_button_collor (id INT AUTO_INCREMENT NOT NULL, slot_item_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, color_id VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_9B46C528FC6930FA (slot_item_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE slot ADD CONSTRAINT FK_AC0E206726F7A36F FOREIGN KEY (slot_badge_id) REFERENCES slot_badge (id)');
        $this->addSql('ALTER TABLE slot_background_color ADD CONSTRAINT FK_5369E12059E5119C FOREIGN KEY (slot_id) REFERENCES slot (id)');
        $this->addSql('ALTER TABLE slot_item ADD CONSTRAINT FK_C1328F8959E5119C FOREIGN KEY (slot_id) REFERENCES slot (id)');
        $this->addSql('ALTER TABLE slot_item_button_collor ADD CONSTRAINT FK_9B46C528FC6930FA FOREIGN KEY (slot_item_id) REFERENCES slot_item (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE slot_background_color DROP FOREIGN KEY FK_5369E12059E5119C');
        $this->addSql('ALTER TABLE slot_item DROP FOREIGN KEY FK_C1328F8959E5119C');
        $this->addSql('ALTER TABLE slot DROP FOREIGN KEY FK_AC0E206726F7A36F');
        $this->addSql('ALTER TABLE slot_item_button_collor DROP FOREIGN KEY FK_9B46C528FC6930FA');
        $this->addSql('DROP TABLE slot');
        $this->addSql('DROP TABLE slot_background_color');
        $this->addSql('DROP TABLE slot_badge');
        $this->addSql('DROP TABLE slot_item');
        $this->addSql('DROP TABLE slot_item_button_collor');
    }
}
