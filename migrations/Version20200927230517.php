<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200927230517 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE slot_item ADD button_color_id INT DEFAULT NULL, ADD button_text VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE slot_item ADD CONSTRAINT FK_C1328F89B0B79E62 FOREIGN KEY (button_color_id) REFERENCES slot_item_button_collor (id)');
        $this->addSql('CREATE INDEX IDX_C1328F89B0B79E62 ON slot_item (button_color_id)');
        $this->addSql('ALTER TABLE slot_item_button_collor DROP FOREIGN KEY FK_9B46C528FC6930FA');
        $this->addSql('DROP INDEX UNIQ_9B46C528FC6930FA ON slot_item_button_collor');
        $this->addSql('ALTER TABLE slot_item_button_collor DROP slot_item_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE slot_item DROP FOREIGN KEY FK_C1328F89B0B79E62');
        $this->addSql('DROP INDEX IDX_C1328F89B0B79E62 ON slot_item');
        $this->addSql('ALTER TABLE slot_item DROP button_color_id, DROP button_text');
        $this->addSql('ALTER TABLE slot_item_button_collor ADD slot_item_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE slot_item_button_collor ADD CONSTRAINT FK_9B46C528FC6930FA FOREIGN KEY (slot_item_id) REFERENCES slot_item (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9B46C528FC6930FA ON slot_item_button_collor (slot_item_id)');
    }
}
