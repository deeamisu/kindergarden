<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200927231010 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE slot ADD background_color_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE slot ADD CONSTRAINT FK_AC0E2067A1A51272 FOREIGN KEY (background_color_id) REFERENCES slot_background_color (id)');
        $this->addSql('CREATE INDEX IDX_AC0E2067A1A51272 ON slot (background_color_id)');
        $this->addSql('ALTER TABLE slot_background_color DROP FOREIGN KEY FK_5369E12059E5119C');
        $this->addSql('DROP INDEX UNIQ_5369E12059E5119C ON slot_background_color');
        $this->addSql('ALTER TABLE slot_background_color DROP slot_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE slot DROP FOREIGN KEY FK_AC0E2067A1A51272');
        $this->addSql('DROP INDEX IDX_AC0E2067A1A51272 ON slot');
        $this->addSql('ALTER TABLE slot DROP background_color_id');
        $this->addSql('ALTER TABLE slot_background_color ADD slot_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE slot_background_color ADD CONSTRAINT FK_5369E12059E5119C FOREIGN KEY (slot_id) REFERENCES slot (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5369E12059E5119C ON slot_background_color (slot_id)');
    }
}
