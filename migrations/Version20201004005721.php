<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201004005721 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE slot_item ADD menu_item_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE slot_item ADD CONSTRAINT FK_C1328F899AB44FE0 FOREIGN KEY (menu_item_id) REFERENCES menu_item (id)');
        $this->addSql('CREATE INDEX IDX_C1328F899AB44FE0 ON slot_item (menu_item_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE slot_item DROP FOREIGN KEY FK_C1328F899AB44FE0');
        $this->addSql('DROP INDEX IDX_C1328F899AB44FE0 ON slot_item');
        $this->addSql('ALTER TABLE slot_item DROP menu_item_id');
    }
}
