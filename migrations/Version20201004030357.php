<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201004030357 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE wow_slider_item ADD image_id INT DEFAULT NULL, DROP image');
        $this->addSql('ALTER TABLE wow_slider_item ADD CONSTRAINT FK_AAD760D3DA5256D FOREIGN KEY (image_id) REFERENCES wow_slider_item_image (id)');
        $this->addSql('CREATE INDEX IDX_AAD760D3DA5256D ON wow_slider_item (image_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE wow_slider_item DROP FOREIGN KEY FK_AAD760D3DA5256D');
        $this->addSql('DROP INDEX IDX_AAD760D3DA5256D ON wow_slider_item');
        $this->addSql('ALTER TABLE wow_slider_item ADD image VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, DROP image_id');
    }
}
