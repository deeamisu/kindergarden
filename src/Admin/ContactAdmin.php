<?php
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 10/4/2020
 * Time: 12:38 AM
 */

namespace App\Admin;

use App\Entity\Contact;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class ContactAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', TextType::class,['label'=>'Nume'])
            ->add('subject', TextType::class,['label'=>'Subiect'])
            ->add('message',TextType::class,['label'=>'Mesaj'])
            ->add('email',TextType::class,['label'=>'Email'])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('subject')
            ->add('name')
            ->add('message')
            ->add('email')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('subject',null,['label'=>'Subiect'])
            ->add('name',null,['label'=>'Nume'])
            ->add('message',null,['label'=>'Mesaj'])
            ->add('email',null,['label'=>'Email'])
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureShowFields(ShowMapper $show)
    {
        $show->with('Mesaj', ['class' => 'col-md-6'])
            ->add('name', TextType::class,['label'=>'Nume'])
            ->add('subject', TextType::class,['label'=>'Subiect'])
            ->add('message',TextType::class,['label'=>'Mesaj'])
            ->add('email',TextType::class,['label'=>'Email'])
            ->end()
        ;

    }

    public function toString($object)
    {
        return $object instanceof Contact
            ? $object->getSubject()
            : 'Contact'; // shown in the breadcrumb on the create view
    }
}