<?php

declare(strict_types=1);

namespace App\Admin;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

final class ContentAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('urlName')
            ->add('title')
            ->add('keywords')
            ->add('description')
            ->add('content')
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('urlName', null, ['label'=>'Url'])
            ->add('title', null, ['label'=>'Titlu'])
            ->add('keywords', null, ['label'=>'Cuvinte cheie'])
            ->add('description', null, ['label'=>'Descriere'])
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('urlName', null, ['label'=>'Url'])
            ->add('title', null, ['label'=>'Titlu'])
            ->add('keywords', null, ['label'=>'Cuvinte cheie'])
            ->add('description', TextareaType::class, ['label'=>'Descriere'])
            ->add('content', CKEditorType::class, [
                'enable' => true,
                'label' => 'Continut'
            ])
            ;
    }
}
