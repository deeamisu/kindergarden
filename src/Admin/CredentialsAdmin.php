<?php
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 10/1/2020
 * Time: 2:00 AM
 */

namespace App\Admin;

use App\Entity\Credentials;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class CredentialsAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('adress', TextType::class)
            ->add('phone', TextType::class)
            ->add('email', TextType::class)
            ->add('site', TextType::class)
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('adress')
            ->add('phone')
            ->add('email')
            ->add('site')
        ;
    }

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('adress')
            ->addIdentifier('phone')
            ->addIdentifier('email')
            ->addIdentifier('site')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureShowFields(ShowMapper $show)
    {
        $show->with('Content', ['class' => 'col-md-6'])
            ->add('adress')
            ->add('phone')
            ->add('email')
            ->add('site' )
            ->end()
        ;
    }

    public function toString($object)
    {
        return $object instanceof Credentials
            ? $object->getAdress().' '.$object->getPhone().' '.$object->getEmail().' '.$object->getSite()
            : 'Credentials'; // shown in the breadcrumb on the create view
    }
}