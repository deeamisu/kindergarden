<?php

namespace App\Admin;
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 10/1/2020
 * Time: 12:29 AM
 */
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\Entity\Gallery;

final class GalleryAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', TextType::class,['label'=>'Nume'])
            ->add('descriptions',TextType::class,['label'=>'Descriere'])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('descriptions')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('descriptions')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureShowFields(ShowMapper $show)
    {
        $show->with('Gallery', [
            'class' => 'col-md-6'
        ])
            ->add('name')
            ->add('descriptions')
        ->end()
        ;
    }

    public function toString($object)
    {
        return $object instanceof Gallery
            ? $object->getName()
            : 'Gallery'; // shown in the breadcrumb on the create view
    }

}