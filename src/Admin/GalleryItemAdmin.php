<?php
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 10/1/2020
 * Time: 1:13 AM
 */

namespace App\Admin;

use App\Entity\Gallery;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use App\Entity\GalleryItem;


final class GalleryItemAdmin extends AbstractAdmin
{
    public $rootDirectory;

    public function __construct($code, $class, $baseControllerName = null, $rootDirectory = null)
    {
        $this->rootDirectory = $rootDirectory;
        parent::__construct($code, $class, $baseControllerName);
    }

    public function getRootDirectory() 
    {
        return $this->rootDirectory;
    }

    protected function configureFormFields(FormMapper $form)
    {
        $fileFormOptions = $this->imagePreview();
            $form
            ->with('Imagine', ['class' => 'col-md-9'])
                ->add('file', FileType::class, $fileFormOptions)
            ->end()
            ->with('Galerie', ['class' => 'col-md-3'])
                ->add('gallery', ModelType::class,[
                    'class' => Gallery::class,
                    'property' => 'name',
                    'label' =>'Galerie'
                ])
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('image')
            ->add('gallery', null, [], EntityType::class, [
                'class' => Gallery::class,
                'choice_label' => 'name',
            ])
        ;
    }

    protected function configureListFields(ListMapper $list)
    {
        $list->add('preview', null, ['label'=>'Imagine', 'template' => 'Admin/GalleryItemAdmin/list_image.html.twig'])
            ->add('gallery.name',null,['label'=>'Galerie'])
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureShowFields(ShowMapper $show)
    {
        $show
            ->with('Content', ['class' => 'col-md-6'])
            ->add('image')
            ->add('gallery', ModelType::class,[
                'class' => Gallery::class,
                'property' => 'name'
            ])
            ->end()
        ;
    }

    public function toString($object)
    {
        return $object instanceof GalleryItem
            ? $object->getImage()
            : 'GalleryItem'; // shown in the breadcrumb on the create view
    }

    public function prePersist($image)
    {
        $this->manageFileUpload($image);
    }

    public function preUpdate($image)
    {
        $this->manageFileUpload($image);
    }

    /**
     * @param $image GalleryItem
     */
    private function manageFileUpload(GalleryItem $image)
    {
        if ($image->getFile()) {
            $image->setRootDirectory($this->getRootDirectory());
            $image->refreshUpdated();
        }
    }

    private function imagePreview()
    {
        /**
         * @var $item GalleryItem
         */
        $item = $this->getSubject();
        $fileFormOptions = ['required' => false,'label'=>'Imagine'];
        if ($item && ($webPath = 'galery/'.$item->getImage())) {
            // get the container so the full path to the image can be set
            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$webPath;

            // add a 'help' option containing the preview's img tag
            $fileFormOptions['help'] = '<img src="'.$fullPath.'" class="admin-preview" style="max-height: 100px; max-width: 100px;"/>';
            $fileFormOptions['help_html'] = true;
        }
        return $fileFormOptions;
    }

    private function imagePreviewFullPath()
    {
        /**
         * @var $item GalleryItem
         */
        $item = $this->getSubject();
        dd($item);
        $webPath = 'galery/'.$item->getImage();
        $container = $this->getConfigurationPool()->getContainer();
        $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$webPath;
        return $fullPath;
    }
}