<?php
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 10/4/2020
 * Time: 12:54 AM
 */

namespace App\Admin;

use App\Entity\MenuItem;
use App\Entity\Slot;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class MenuItemAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', null, ['label'=>'Nume'])
            ->add('itemRoute',null, ['label'=>'Url'])
            ->add('page',null, ['label'=>'Pagina'])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name',null, ['label'=>'Nume'])
            ->add('itemRoute',null, ['label'=>'Url'])
            ->add('page',null, ['label'=>'Pagina'])
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    public function toString($object)
    {
        return $object instanceof MenuItem
            ? $object->getName()
            : 'Menu Item'; // shown in the breadcrumb on the create view
    }
}