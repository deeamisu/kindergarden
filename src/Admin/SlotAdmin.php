<?php

declare(strict_types=1);

namespace App\Admin;

use App\Entity\Content;
use App\Entity\MenuItem;
use App\Entity\Slot;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Show\ShowMapper;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
final class SlotAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('urlName')
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('name', null, ['label'=>'Nume'])
            ->add('urlName', null, ['label'=>'Url'])
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('name', null, ['label'=>'Nume'])
            ->add('urlName', null, ['label'=>'Url', 'required'=>false])
            ->add('content', CKEditorType::class, [
                'enable' => true,
                'label' => 'Continut'
            ])
            ;
    }

    public function toString($object)
    {
        return $object instanceof Slot
            ? $object->getName()
            : 'Slot'; // shown in the breadcrumb on the create view
    }
}
