<?php
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 10/4/2020
 * Time: 3:05 AM
 */

namespace App\Admin;

use App\Entity\SlotBackgroundColor;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class SlotBackgroundAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', TextType::class);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name')
            ->add('slots')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name')
            ->add('slots')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureShowFields(ShowMapper $show)
    {
        $show->with('Content' , ['class' => 'col-md-6'])
                ->add('name')
                ->add('slots')
            ->end()
            ;
    }

    public function toString($object)
    {
        return $object instanceof SlotBackgroundColor
            ? $object->getName()
            : 'Slot Background'; // shown in the breadcrumb on the create view
    }
}