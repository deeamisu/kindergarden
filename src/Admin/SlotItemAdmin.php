<?php
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 10/4/2020
 * Time: 3:38 AM
 */

namespace App\Admin;

use App\Entity\MenuItem;
use App\Entity\Slot;
use App\Entity\SlotItem;
use App\Entity\SlotItemButtonCollor;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class SlotItemAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('title', TextType::class)
            ->add('text', CKEditorType::class, [
                'enable' => true
            ])
            //->add('buttonText', TextType::class)
//            ->add('buttonColor', ModelType::class,[
//                'class' => SlotItemButtonCollor::class,
//                'property' => 'name'
//            ])
//            ->add('menuItem', ModelType::class,[
//                'class' => MenuItem::class,
//                'property' => 'name'
//            ])
//            ->add('route')
            ->add('slot', ModelType::class,[
                'class' => Slot::class,
                'property' => 'name'
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title')
            ->add('slot.name')
            ->add('menuItem')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('title')
            ->add('text')
            ->addIdentifier('slot.name')
            //->add('buttonColor')
            //->add('buttonText')
            //->add('menuItem')
            //->add('route')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureShowFields(ShowMapper $show)
    {
        $show
            ->add('title')
            ->add('text')
            ->add('slot')
            ->add('buttonColor')
            ->add('buttonText')
            ->add('menuItem')
            ->add('route')
        ;
    }

    public function toString($object)
    {
        return $object instanceof SlotItem
            ? $object->getTitle()
            : 'Slot Item'; // shown in the breadcrumb on the create view
    }
}