<?php
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 10/4/2020
 * Time: 5:27 AM
 */

namespace App\Admin;

use App\Entity\SlotItemButtonCollor;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;

final class SlotItemButtonCollorAdmin extends AbstractAdmin
{
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name')
        ->add('_action', null, [
        'actions' => [
            'show' => [],
            'edit' => [],
            'delete' => [],
        ],
    ]);
    }

    public function toString($object)
    {
        return $object instanceof SlotItemButtonCollor
            ? $object->getName()
            : 'Button Color'; // shown in the breadcrumb on the create view
    }
}