<?php
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 10/4/2020
 * Time: 6:27 AM
 */

namespace App\Admin;

use App\Entity\WowSliderItemImage;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\FileType;

final class WowSliderImageAdmin extends AbstractAdmin
{
    public $rootDirectory;

    public function __construct($code, $class, $baseControllerName = null, $rootDirectory = null)
    {
        $this->rootDirectory = $rootDirectory;
        parent::__construct($code, $class, $baseControllerName);
    }

    public function getRootDirectory()
    {
        return $this->rootDirectory;
    }

    /**
     * @param FormMapper $form
     */
    protected function configureFormFields(FormMapper $form)
    {
        $fileFormOptions = $this->imagePreview();
        $form
            ->with('Content', ['class' => 'col-md-9'])
            ->add('file', FileType::class, $fileFormOptions)
            ->end()
        ;
    }


    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name')
            //->add('preview', null, ['template' => 'Admin/GalleryItemAdmin/list_image.html.twig'])
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    public function toString($object): ?string
    {
        return $object instanceof WowSliderItemImage
            ? $object->getName()
            : 'Wow Slider Item Image'; // shown in the breadcrumb on the create view
    }

    public function prePersist($image)
    {
        $this->manageFileUpload($image);
    }

    public function preUpdate($image)
    {
        $this->manageFileUpload($image);
    }

    /**
     * @param $image WowSliderItemImage
     */
    private function manageFileUpload($image)
    {
        if ($image->getFile()) {
            $image->setRootDirectory($this->getRootDirectory());
            $image->refreshUpdated();
        }
    }

    private function imagePreview()
    {
        /**
         * @var $item WowSliderItemImage
         */
        $item = $this->getSubject();
        $fileFormOptions = ['required' => false];
        if ($item && ($webPath = 'kindergarden_template/HTML/images/wowslider/'.$item->getImage())) {
            // get the container so the full path to the image can be set
            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$webPath;

            // add a 'help' option containing the preview's img tag
            $fileFormOptions['help'] = '<img src="'.$fullPath.'" class="admin-preview" style="max-height: 100px; max-width: 100px;"/>';
            $fileFormOptions['help_html'] = true;
        }
        return $fileFormOptions;
    }

    private function imagePreviewFullPath()
    {
        /**
         * @var $item WowSliderItemImage
         */
        $item = $this->getSubject();
        dd($item);
        $webPath = 'kindergarden_template/HTML/images/wowslider/'.$item->getImage();
        $container = $this->getConfigurationPool()->getContainer();
        $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$webPath;
        return $fullPath;
    }
}