<?php
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 10/4/2020
 * Time: 6:11 AM
 */

namespace App\Admin;

use App\Entity\WowSliderItem;
use App\Entity\WowSliderItemImage;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class WowSliderItemAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Content',[
                'class' => "col-md-9"
            ])
                ->add('title', TextType::class,['label'=>'Titlu'])
                ->add('description', TextType::class,['label'=>'Descriere'])
                ->add('link', TextType::class,['label'=>'Url'])
            ->end()
            ->with('Subscribed', [
                'class' => 'col-md-3'
            ])
                ->add('image', ModelType::class, [
                    'class' => WowSliderItemImage::class,
                    'property' => 'name',
                    'label'=>'Imagine'
                ])
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title')
            ->add('description')
            ->add('image')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('title', null, ['label'=>'Titlu'])
            ->add('link', null, ['label'=>'Link'])
            ->add('description', null, ['label'=>'Descriere'])
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    public function toString($object)
    {
        return $object instanceof WowSliderItem
            ? $object->getTitle()
            : 'Wow Slider Item'; // shown in the breadcrumb on the create view
    }
}