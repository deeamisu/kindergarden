<?php

namespace App\Command;

use App\Service\GeneralService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class MakeUserCommand extends Command
{
    protected static $defaultName = 'app:make-user';

    private $generalService;

    /**
     * MakeUserCommand constructor.
     * @param $generalService
     */
    public function __construct(GeneralService $generalService)
    {
        parent::__construct();
        $this->generalService = $generalService;
    }


    protected function configure()
    {
        $this
            ->setDescription('Creates a new App User')
            ->addArgument('username', InputArgument::REQUIRED, 'New user username ')
            ->addArgument('password', InputArgument::REQUIRED, 'New user password')
            //->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Creating new user');

        $username = $input->getArgument('username');
        $password = $input->getArgument('password');
        $user = $this->generalService->newUser($username,$password);

        $io->success('You have created '.$user->getUsername().' !');

        return 0;
    }
}
