<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Entity\Content;
use App\Entity\Credentials;
use App\Entity\Gallery;
use App\Entity\MenuItem;
use App\Entity\Slot;
use App\Entity\WowSliderItem;
use App\Service\GeneralService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DefaultController
 * @package App\Controller
 */
class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default")
     */
    public function index()
    {
        $home = $this->getDoctrine()->getRepository(MenuItem::class)->findOneBy(['itemRoute' => 'default']);
        $wowSliderItems = $this->getDoctrine()->getRepository(WowSliderItem::class)->findAll();
        return $this->render('default/index.html.twig',[
            'wowSliderItems' => $wowSliderItems,
        ]);
    }

    /**
     * @Route("/galerie", name="gallery")
     */
    public function gallery()
    {
        $galleries = $this->getDoctrine()->getRepository(Gallery::class)->findAll();
        return $this->render('default/gallery.html.twig',[
            'galleries' => $galleries,
        ]);
    }

    /**
     * @Route("/contact", name="contact" , methods={"GET","POST"})
     */
    public function contactMessage(Request $request, GeneralService $generalService,MailerInterface $mailer)
    {
        if ($request->getMethod() == "POST") {
            $entityManager = $this->getDoctrine()->getManager();
            $message = new Contact();
            $message->setName($_POST['name']);
            $message->setEmail($_POST['email']);
            $message->setSubject($_POST['subject']);
            $message->setMessage($_POST['message']);
            $file = time().rand(1000,9999).'.'.pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            $path = __DIR__.'/../../files/'.$file;
            $message->setFile($file);
            $entityManager->persist($message);
            $entityManager->flush();
            move_uploaded_file($_FILES['file']['tmp_name'], $path);
            $generalService->sendEmail($message, $path, $file);
        }
        $credentials = $this->getDoctrine()->getRepository(Credentials::class)->findAll();
        return $this->render('default/contact.html.twig',[
            'credentials' => $credentials,
        ]);
    }



    /**
     * @Route("/continut/{urlName}", name="content")
     * @param $route
     * @return Response
     */
    public function contentAction($urlName): Response
    {

        $content = $this->getDoctrine()->getRepository(Content::class)->findOneBy(['urlName' => $urlName]);
        return $this->render('@templates/default/content.html.twig', [
            'content' => $content,
        ]);
    }
}
