<?php

namespace App\Entity;

use App\Repository\GalleryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GalleryRepository::class)
 */
class Gallery
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $descriptions;

    /**
     * @ORM\OneToMany(targetEntity=GalleryItem::class, mappedBy="gallery")
     */
    private $galleryItems;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescriptions(): ?string
    {
        return $this->descriptions;
    }

    public function setDescriptions(string $descriptions): self
    {
        $this->descriptions = $descriptions;

        return $this;
    }

    /**
     * @return Collection|GalleryItem[]
     */
    public function getGalleryItems(): Collection
    {
        return $this->galleryItems;
    }

    public function addGalleryItem(GalleryItem $galleryItem): self
    {
        if (!$this->galleryItems->contains($galleryItem)) {
            $this->galleryItems[] = $galleryItem;
            $galleryItem->setGallery($this);
        }

        return $this;
    }

    public function removeGalleryItem(GalleryItem $galleryItem): self
    {
        if ($this->galleryItems->contains($galleryItem)) {
            $this->galleryItems->removeElement($galleryItem);
            // set the owning side to null (unless already changed)
            if ($galleryItem->getGallery() === $this) {
                $galleryItem->setGallery(null);
            }
        }

        return $this;
    }

    function __toString()
    {
        return $this->getName();
    }


}
