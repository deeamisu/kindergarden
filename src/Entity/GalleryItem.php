<?php

namespace App\Entity;

use App\Repository\GalleryItemRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass=GalleryItemRepository::class)
 */
class GalleryItem
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity=Gallery::class, inversedBy="galleryItems")
     */
    private $gallery;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated;

    private $file;

    private $rootDirectory;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fullImagePath;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getGallery(): ?Gallery
    {
        return $this->gallery;
    }

    public function setGallery(?Gallery $gallery): self
    {
        $this->gallery = $gallery;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(?\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }

        // we use the original file name here but you should
        // sanitize it at least to avoid any security issues

        // move takes the target directory and target filename as params
        $this->getFile()->move($this->getRootDirectory().'/public/galery',
            $this->getFile()->getClientOriginalName()
        );

        // set the path property to the filename where you've saved the file
        $this->setImage($this->getFile()->getClientOriginalName());

        $this->setFullImagePath($this->getRootDirectory().'/public/galery/'.$this->getImage());

        $filesystem = new Filesystem();
        $filesystem->copy($this->getRootDirectory().'/public/galery/'.$this->getImage(),
            $this->getRootDirectory().'/public/gallery_slide/'. $this->getImage());

        // clean up the file property as you won't need it anymore
        $this->setFile(null);
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function lifecycleFileUpload()
    {
        $this->upload();
    }

    /**
     * Updates the hash value to force the preUpdate and postUpdate events to fire.
     */
    public function refreshUpdated()
    {
        $this->setUpdated(new \DateTime());
    }

    /**
     * @return mixed
     */
    public function getRootDirectory()
    {
        return $this->rootDirectory;
    }

    /**
     * @param mixed $rootDirectory
     */
    public function setRootDirectory($rootDirectory)
    {
        $this->rootDirectory = $rootDirectory;
    }

    public function getFullImagePath(): ?string
    {
        return $this->fullImagePath;
    }

    public function setFullImagePath(?string $fullImagePath): self
    {
        $this->fullImagePath = $fullImagePath;

        return $this;
    }

}
