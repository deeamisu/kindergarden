<?php

namespace App\Entity;

use App\Repository\MenuItemRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MenuItemRepository::class)
 */
class MenuItem
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $itemRoute;

    /**
     * @ORM\ManyToOne(targetEntity=Content::class, inversedBy="menuItems")
     */
    private $page;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    function __toString()
    {
        return (string)$this->getName();
    }

    public function getItemRoute(): ?string
    {
        return $this->itemRoute;
    }

    public function setItemRoute(string $itemRoute): self
    {
        $this->itemRoute = $itemRoute;

        return $this;
    }

    public function getPage(): ?Content
    {
        return $this->page;
    }

    public function setPage(?Content $page): self
    {
        $this->page = $page;

        return $this;
    }

}
