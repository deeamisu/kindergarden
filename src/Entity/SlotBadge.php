<?php

namespace App\Entity;

use App\Repository\SlotBadgeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SlotBadgeRepository::class)
 */
class SlotBadge
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Slot::class, mappedBy="slotBadge")
     */
    private $slot;

    public function __construct()
    {
        $this->slot = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Slot[]
     */
    public function getSlot(): Collection
    {
        return $this->slot;
    }

    public function addSlot(Slot $slot): self
    {
        if (!$this->slot->contains($slot)) {
            $this->slot[] = $slot;
            $slot->setSlotBadge($this);
        }

        return $this;
    }

    public function removeSlot(Slot $slot): self
    {
        if ($this->slot->contains($slot)) {
            $this->slot->removeElement($slot);
            // set the owning side to null (unless already changed)
            if ($slot->getSlotBadge() === $this) {
                $slot->setSlotBadge(null);
            }
        }

        return $this;
    }

    function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->getName();
    }


}
