<?php

namespace App\Entity;

use App\Repository\SlotItemButtonCollorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SlotItemButtonCollorRepository::class)
 */
class SlotItemButtonCollor
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=SlotItem::class, mappedBy="buttonColor")
     */
    private $slotItems;

    public function __construct()
    {
        $this->slotItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|SlotItem[]
     */
    public function getSlotItems(): Collection
    {
        return $this->slotItems;
    }

    public function addSlotItem(SlotItem $slotItem): self
    {
        if (!$this->slotItems->contains($slotItem)) {
            $this->slotItems[] = $slotItem;
            $slotItem->setButtonColor($this);
        }

        return $this;
    }

    public function removeSlotItem(SlotItem $slotItem): self
    {
        if ($this->slotItems->contains($slotItem)) {
            $this->slotItems->removeElement($slotItem);
            // set the owning side to null (unless already changed)
            if ($slotItem->getButtonColor() === $this) {
                $slotItem->setButtonColor(null);
            }
        }

        return $this;
    }

    function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->getName();
    }


}
