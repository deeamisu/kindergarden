<?php

namespace App\Entity;

use App\Repository\WowSliderItemImageRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass=WowSliderItemImageRepository::class)
 */
class WowSliderItemImage
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=WowSliderItem::class, mappedBy="image")
     */
    private $wowSliderItems;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated;

    private $file;

    private $rootDirectory;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fullImagePath;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $url;

    public function __construct()
    {
        $this->wowSliderItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|WowSliderItem[]
     */
    public function getWowSliderItems(): Collection
    {
        return $this->wowSliderItems;
    }

    public function addWowSliderItem(WowSliderItem $wowSliderItem): self
    {
        if (!$this->wowSliderItems->contains($wowSliderItem)) {
            $this->wowSliderItems[] = $wowSliderItem;
            $wowSliderItem->setImage($this);
        }

        return $this;
    }

    public function removeWowSliderItem(WowSliderItem $wowSliderItem): self
    {
        if ($this->wowSliderItems->contains($wowSliderItem)) {
            $this->wowSliderItems->removeElement($wowSliderItem);
            // set the owning side to null (unless already changed)
            if ($wowSliderItem->getImage() === $this) {
                $wowSliderItem->setImage(null);
            }
        }

        return $this;
    }

    function __toString()
    {
        return $this->getName();
    }

    public function setUpdated(?DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    public function upload()
        {
            // the file property can be empty if the field is not required
            if (null === $this->getFile()) {
                return;
            }

            // we use the original file name here but you should
            // sanitize it at least to avoid any security issues

            // move takes the target directory and target filename as params
            $this->getFile()->move($this->getRootDirectory().'/public/kindergarden_template/HTML/images/wowslider',
                $this->getFile()->getClientOriginalName()
            );

            // set the path property to the filename where you've saved the file
            $newFilename = time().'.png';
            $this->addPNGMask(
                $this->getRootDirectory().'/public/kindergarden_template/HTML/images/wowslider/'.$this->getFile()->getClientOriginalName(),
                $this->getRootDirectory().'/public/kindergarden_template/HTML/images/wowslider/'.$newFilename
            );
            $this->setFullImagePath($this->getRootDirectory().'/public/kindergarden_template/HTML/images/wowslider/'.$this->getFile()->getClientOriginalName());

            $this->setImage($newFilename);
            $this->setName($this->getImage());

            // clean up the file property as you won't need it anymore
            $this->setFile(null);
        }

    function addPNGMask($sourcePath, $destinationPath){
        // Load source and mask
        $this->cropToFit($sourcePath, 670, 404);

        $source = imagecreatefromjpeg( $sourcePath );
        //dd($sourcePath, $destinationPath, $source);
        //dd($this->getRootDirectory().'/public/mask.png');
        $mask = imagecreatefrompng( '/home/ianosidvps/webapps/s40apps/kindergarden/public/mask.png' );
        // Apply mask to source
        $this->imagealphamask( $source, $mask );


        imagepng( $source,  $destinationPath);
    }

    function imagealphamask( &$picture, $mask ) {
        // Get sizes and set up new picture
        $xSize = imagesx( $picture );
        $ySize = imagesy( $picture );
        $newPicture = imagecreatetruecolor( $xSize, $ySize );
        imagesavealpha( $newPicture, true );
        imagefill( $newPicture, 0, 0, imagecolorallocatealpha( $newPicture, 0, 0, 0, 127 ) );

        // Perform pixel-based alpha map application
        for( $x = 0; $x < $xSize; $x++ ) {
            for( $y = 0; $y < $ySize; $y++ ) {
                $alpha = imagecolorsforindex( $mask, imagecolorat( $mask, $x, $y ) );

                $color = imagecolorsforindex( $picture, imagecolorat( $picture, $x, $y ) );
                $alpha = 127 - floor((127-$color['alpha']) * ($alpha[ 'red' ]/255));
                //imagesetpixel( $newPicture, $x, $y, imagecolorallocatealpha( $newPicture, $color[ 'red' ], $color[ 'green' ], $color[ 'blue' ], $alpha ) );
                imagesetpixel( $newPicture, $x, $y, imagecolorallocatealpha( $newPicture, $color[ 'red' ], $color[ 'green' ], $color[ 'blue' ], 127 - $alpha ) );
            }
        }

        // Copy back to original picture
        imagedestroy( $picture );
        $picture = $newPicture;
    }

    function cropToFit($source_path, $width, $height)
    {
        /*
         * Add file validation code here
         */

        list($source_width, $source_height, $source_type) = getimagesize($source_path);

        switch ($source_type) {
            case IMAGETYPE_GIF:
                $source_gdim = imagecreatefromgif($source_path);
                break;
            case IMAGETYPE_JPEG:
                $source_gdim = imagecreatefromjpeg($source_path);
                break;
            case IMAGETYPE_PNG:
                $source_gdim = imagecreatefrompng($source_path);
                break;
        }

        $source_aspect_ratio = $source_width / $source_height;
        $desired_aspect_ratio = $width / $height;

        if ($source_aspect_ratio > $desired_aspect_ratio) {
            /*
             * Triggered when source image is wider
             */
            $temp_height = $height;
            $temp_width = ( int ) ($height * $source_aspect_ratio);
        } else {
            /*
             * Triggered otherwise (i.e. source image is similar or taller)
             */
            $temp_width = $width;
            $temp_height = ( int ) ($width / $source_aspect_ratio);
        }

        /*
         * Resize the image into a temporary GD image
         */

        $temp_gdim = imagecreatetruecolor($temp_width, $temp_height);
        imagecopyresampled(
            $temp_gdim,
            $source_gdim,
            0, 0,
            0, 0,
            $temp_width, $temp_height,
            $source_width, $source_height
        );

        /*
         * Copy cropped region from temporary image into the desired GD image
         */

        $x0 = ($temp_width - $width) / 2;
        $y0 = ($temp_height - $height) / 2;
        $desired_gdim = imagecreatetruecolor($width, $height);
        imagecopy(
            $desired_gdim,
            $temp_gdim,
            0, 0,
            $x0, $y0,
            $width, $height
        );

        imagejpeg($desired_gdim, $source_path);

        /*
         * Add clean-up code here
         */
    }


    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function lifecycleFileUpload()
    {
        $this->upload();
    }

    /**
     * Updates the hash value to force the preUpdate and postUpdate events to fire.
     */
    public function refreshUpdated()
    {
        $this->setUpdated(new DateTime());
    }

    /**
     * @return mixed
     */
    public function getRootDirectory()
    {
        return $this->rootDirectory;
    }

    /**
     * @param mixed $rootDirectory
     */
    public function setRootDirectory($rootDirectory)
    {
        $this->rootDirectory = $rootDirectory;
    }

    public function getFullImagePath(): ?string
    {
        return $this->fullImagePath;
    }

    public function setFullImagePath(?string $fullImagePath): self
    {
        $this->fullImagePath = $fullImagePath;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     * @return WowSliderItemImage
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }


}
