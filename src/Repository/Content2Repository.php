<?php

namespace App\Repository;

use App\Entity\Content2;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Content2|null find($id, $lockMode = null, $lockVersion = null)
 * @method Content2|null findOneBy(array $criteria, array $orderBy = null)
 * @method Content2[]    findAll()
 * @method Content2[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class Content2Repository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Content2::class);
    }

    // /**
    //  * @return Content2[] Returns an array of Content2 objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Content2
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
