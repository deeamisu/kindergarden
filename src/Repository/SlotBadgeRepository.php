<?php

namespace App\Repository;

use App\Entity\SlotBadge;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SlotBadge|null find($id, $lockMode = null, $lockVersion = null)
 * @method SlotBadge|null findOneBy(array $criteria, array $orderBy = null)
 * @method SlotBadge[]    findAll()
 * @method SlotBadge[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SlotBadgeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SlotBadge::class);
    }

    // /**
    //  * @return SlotBadgeAdmin[] Returns an array of SlotBadgeAdmin objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SlotBadge
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
