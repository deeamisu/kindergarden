<?php

namespace App\Repository;

use App\Entity\SlotItemButtonCollor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SlotItemButtonCollor|null find($id, $lockMode = null, $lockVersion = null)
 * @method SlotItemButtonCollor|null findOneBy(array $criteria, array $orderBy = null)
 * @method SlotItemButtonCollor[]    findAll()
 * @method SlotItemButtonCollor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SlotItemButtonCollorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SlotItemButtonCollor::class);
    }

    // /**
    //  * @return SlotItemButtonCollor[] Returns an array of SlotItemButtonCollor objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SlotItemButtonCollor
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
