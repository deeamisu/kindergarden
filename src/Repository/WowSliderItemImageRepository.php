<?php

namespace App\Repository;

use App\Entity\WowSliderItemImage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WowSliderItemImage|null find($id, $lockMode = null, $lockVersion = null)
 * @method WowSliderItemImage|null findOneBy(array $criteria, array $orderBy = null)
 * @method WowSliderItemImage[]    findAll()
 * @method WowSliderItemImage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WowSliderItemImageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WowSliderItemImage::class);
    }

    // /**
    //  * @return WowSliderItemImage[] Returns an array of WowSliderItemImage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WowSliderItemImage
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
