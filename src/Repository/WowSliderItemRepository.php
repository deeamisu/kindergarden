<?php

namespace App\Repository;

use App\Entity\WowSliderItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WowSliderItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method WowSliderItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method WowSliderItem[]    findAll()
 * @method WowSliderItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WowSliderItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WowSliderItem::class);
    }

    // /**
    //  * @return WowSliderItem[] Returns an array of WowSliderItem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WowSliderItem
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
