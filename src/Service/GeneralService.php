<?php

namespace App\Service;
use App\Entity\Contact;
use App\Entity\Credentials;
use App\Entity\MenuItem;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;


/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 9/24/2020
 * Time: 3:03 AM
 */
class GeneralService
{
    private $entityManager;

    private $passwordEncoder;

    private $parameterBag;

    private $mailer;

    /**
     * GeneralService constructor.
     * @param $entityManager EntityManagerInterface
     * @param $passwordEncoder UserPasswordEncoderInterface
     * @param $parameterBag ParameterBagInterface
     * @param $mailer MailerInterface
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $passwordEncoder,
        ParameterBagInterface $parameterBag,
        MailerInterface $mailer
    )
    {
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->parameterBag = $parameterBag;
        $this->mailer = $mailer;
    }

    public function getMenuItems()
    {
        return $this->entityManager->getRepository(MenuItem::class)->findAll();
    }

    public function newUser($username, $password)
    {
        $user = new User();
        $user->setUsername($username);
        $user->setPassword($this->passwordEncoder->encodePassword($user,$password));
        $this->entityManager->persist($user);
        $this->entityManager->flush();
        return $user;
    }

    /**
     * @param $contact Contact
     */
    public function sendEmail(Contact $contact, $path, $filename)
    {
        $email = (new TemplatedEmail())
            ->from('noreply@gradinitaconstbrancusi.ro')
            ->to(new Address('cfgradinitacbrancusi@yahoo.com'))
            ->replyTo(new Address($contact->getEmail()))
            ->subject('Mesaj din site')
            ->htmlTemplate('email/contact.html.twig')
            ->context([
                'name' => $contact->getName(),
                'mail' => $contact->getEmail(),
                'subject' => $contact->getSubject(),
                'message' => $contact->getMessage(),
            ])
            ->attachFromPath($path , $filename);

        $this->mailer->send($email);
    }

}