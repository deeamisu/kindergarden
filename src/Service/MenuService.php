<?php

namespace App\Service;

use App\Entity\Content;
use App\Entity\MenuItem;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class MenuService
 * @package App\Service
 */
final class MenuService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /** @var UrlGenerator */
    protected $urlGenerator;

    /**
     * MenuService constructor.
     * @param $em
     */
    public function __construct(EntityManagerInterface $em, UrlGeneratorInterface $urlGenerator)
    {
        $this->em = $em;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * @return array
     */
    public function getMenuItems(): ?array
    {
        return $this->em->getRepository(MenuItem::class)->findAll();
    }

    public function getUrl(MenuItem $menuItem)
    {
        if ($menuItem->getPage()){
            return $this->urlGenerator->generate('content', ['urlName'=>$menuItem->getPage()->getUrlName()]);
        }
        return $this->urlGenerator->generate($menuItem->getItemRoute());
    }
}