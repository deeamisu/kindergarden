<?php


namespace App\Service;


use App\Entity\Slot;
use App\Repository\SlotRepository;

class SlotService
{
    /** @var SlotRepository */
    protected $slotRepository;

    /**
     * SlotService constructor.
     * @param SlotRepository $slotRepository
     */
    public function __construct(SlotRepository $slotRepository)
    {
        $this->slotRepository = $slotRepository;
    }

    public function get($slotName)
    {
        $slot = $this->slotRepository->findOneBy(['name'=>$slotName]);
        if (!$slot){
            $slot = new Slot();
            $slot->setContent("Slot '$slotName' does not exist!!");
        }

        return $slot->getContent();
    }
}